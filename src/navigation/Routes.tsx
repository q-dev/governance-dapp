import { useEffect } from 'react';
import { Route, RouteComponentProps, Switch } from 'react-router-dom';

import * as Sentry from '@sentry/react';
import { ProposalContractType } from 'typings/contracts';

import ErrorBoundary from 'components/Custom/ErrorBoundary';
import DataPrivacy from 'pages/DataPrivacy';
import Governance from 'pages/Governance';
import VotingPower from 'pages/Governance/components/VotingPower';
import NewProposal from 'pages/Governance/NewProposal';
import Proposal from 'pages/Governance/Proposal';
import Imprint from 'pages/Imprint';
import NotFound from 'pages/NotFound';

import { getState } from 'store';

import { RoutePaths } from 'constants/routes';
import { captureError } from 'utils/errors';

function addSentryContext () {
  try {
    const { chainId, loadType } = getState().user;
    Sentry.setContext('additional', {
      network: chainId,
      loadType,
    });
  } catch (error) {
    captureError(error);
  }
}

function Routes () {
  useEffect(() => {
    addSentryContext();
  }, []);

  return (
    <ErrorBoundary>
      <Switch>
        <Route exact path="/governance/:type/new">
          <NewProposal />
        </Route>

        <Route exact path={RoutePaths.votingPower}>
          <VotingPower />
        </Route>

        <Route exact path={['/', RoutePaths.governanceTab]}>
          <Governance />
        </Route>

        <Route
          exact
          path={RoutePaths.proposal}
          component={(props: RouteComponentProps<{ id: string; contract: ProposalContractType }>) => (
            <Proposal {...props} />
          )}
        />

        <Route exact path="/imprint">
          <Imprint />
        </Route>
        <Route exact path="/data-privacy">
          <DataPrivacy />
        </Route>

        <Route component={NotFound} />
      </Switch>
    </ErrorBoundary>
  );
}

export default Routes;
