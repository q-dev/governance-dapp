export { default } from './Tag';
export type TagState = 'pending' | 'approved' | 'rejected';
