import styled from 'styled-components';

export const RangeContainer = styled.div<{
  $disabled: boolean;
  $percent: number;
  $hideInput: boolean;
}>`
  position: relative;
  display: grid;
  gap: 4px;
  align-items: center;

  .range-label {
    color: ${({ theme, $disabled }) => $disabled
      ? theme.colors.disableSecondary
      : theme.colors.textPrimary
    };
  }

  .range-wrapper {
    display: grid;
    grid-template-columns: ${({ $hideInput }) => $hideInput ? '1fr' : '1fr minmax(80px, 10%)'};
    gap: 12px;
  }

  .range-values {
    display: flex;
    justify-content: space-between;
    margin-bottom: 4px;
  }

  .range-value {
    display: flex;
    gap: 4px;
    color: ${({ theme, $disabled }) => $disabled
      ? theme.colors.disableSecondary
      : theme.colors.textPrimary
    };
  }

  .range-input {
    cursor: pointer;
    -webkit-appearance: none;
    width: 100%;
    height: 8px;
    background-color: ${({ theme, $disabled }) => $disabled
        ? theme.colors.disablePrimary
        : theme.colors.tertiaryMain
    };
    border-radius: 10px;
    background-image: linear-gradient(
      ${({ theme }) => theme.colors.secondaryMain},
      ${({ theme }) => theme.colors.secondaryMain}
    );
    background-size: ${({ $percent }) => $percent}% 100%;
    background-repeat: no-repeat;

    &:disabled {
      cursor: not-allowed;
      background-image: linear-gradient(
        ${({ theme }) => theme.colors.disableSecondary},
        ${({ theme }) => theme.colors.disableSecondary}
      );
    }

    &:focus-visible {
      outline: none;
    }

    &:focus-visible::-webkit-slider-runnable-track {
      outline: 2px solid ${({ theme }) => theme.colors.borderMain};
      border-radius: 2px;
    }

    &::-webkit-slider-thumb {
      -webkit-appearance: none;
      width: 20px;
      height: 20px;
      background: ${({ theme }) => theme.colors.tertiaryMain};
      border: 4px solid ${({ theme, $disabled }) => $disabled
        ? theme.colors.disableSecondary
        : theme.colors.secondaryMain
      };
      border-radius: 50%;
    }

    &::-webkit-slider-runnable-track  {
      -webkit-appearance: none;
      box-shadow: none;
      border: none;
      background-color: transparent;
    }
  }

  .range-error {
    color: ${({ theme }) => theme.colors.errorMain};
  }
`;
