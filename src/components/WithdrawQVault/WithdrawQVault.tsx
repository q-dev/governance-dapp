import { useState } from 'react';
import { useTranslation } from 'react-i18next';

import styled from 'styled-components';

import Button from 'ui/Button';
import Modal from 'ui/Modal';

import WithdrawForm from './WithdrawForm';

const StyledWrapper = styled.div`
  display: inline-flex;
  margin-left: 8px;

  .top-up-btn {
    height: 24px;
    border-radius: 8px;
  }
`;

interface Props {
  onSubmit?: () => void;
}

function WithdrawQVault ({ onSubmit = () => {} }: Props) {
  const { t } = useTranslation();
  const [modalOpen, setModalOpen] = useState(false);

  const handleSubmit = () => {
    setModalOpen(false);
    onSubmit();
  };

  return (
    <StyledWrapper>
      <Button
        compact
        icon
        look="secondary"
        className="top-up-btn"
        onClick={() => setModalOpen(true)}
      >
        - {t('WITHDRAW')}
      </Button>

      <Modal
        open={modalOpen}
        title={t('WITHDRAW')}
        tip={t('FROM_Q_VAULT_TO_Q_WALLET')}
        onClose={() => setModalOpen(false)}
      >
        <WithdrawForm onSubmit={handleSubmit} />
      </Modal>
    </StyledWrapper>
  );
}

export default WithdrawQVault;
