import { useEffect, useMemo } from 'react';
import { useTranslation } from 'react-i18next';

import styled from 'styled-components';

import Button from 'ui/Button';
import Input from 'ui/Input';

import useForm from 'hooks/useForm';

import { useQVault } from 'store/q-vault/hooks';
import { useTransaction } from 'store/transaction/hooks';
import { useUser } from 'store/user/hooks';

import { formatAsset, toBigNumber } from 'utils/numbers';
import { amount, required } from 'utils/validators';

const StyledForm = styled.form`
  display: grid;
  gap: 16px;

  .withdraw-form-btn {
    width: 100%;
  }
`;

function WithdrawForm ({ onSubmit }: { onSubmit: () => void }) {
  const { t } = useTranslation();
  const { submitTransaction } = useTransaction();

  const {
    vaultBalance,
    qVaultMinimumTimeLock,
    withdrawFromVault,
    votingWeight,
    isVotingWeightUnlocked,
    delegationStakeInfo,
    loadLockInfo,
    loadDelegationStakeInfo
  } = useQVault();
  const user = useUser();

  useEffect(() => {
    loadLockInfo(user.address);
    loadDelegationStakeInfo();
  }, []);

  const maxAmount = useMemo(() => {
    return toBigNumber(vaultBalance)
      .minus(qVaultMinimumTimeLock)
      .minus(
        isVotingWeightUnlocked || toBigNumber(delegationStakeInfo.totalDelegatedStake).isGreaterThan(votingWeight)
          ? delegationStakeInfo.totalDelegatedStake
          : votingWeight
      )
      .toString();
  }, [
    vaultBalance,
    qVaultMinimumTimeLock,
    votingWeight,
    isVotingWeightUnlocked,
    delegationStakeInfo
  ]);

  const form = useForm({
    initialValues: { amount: '' },
    validators: { amount: [required, amount(maxAmount)] },
    onSubmit: ({ amount }) => {
      submitTransaction({
        successMessage: t('WITHDRAW_FROM_Q_VAULT_SUCCESS'),
        submitFn: async () => withdrawFromVault({ amount, address: user.address }),
        onSuccess: () => onSubmit()
      });
    }
  });

  return (
    <StyledForm
      noValidate
      className="withdraw-form"
      onSubmit={form.submit}
    >
      <Input
        {...form.fields.amount}
        type="number"
        label={t('AMOUNT')}
        prefix="Q"
        max={String(maxAmount)}
        placeholder="0.0"
        hint={t('AVAILABLE_TO_WITHDRAW', { amount: formatAsset(maxAmount, 'Q') })}
      />

      <Button
        type="submit"
        className="withdraw-form-btn"
        disabled={!form.isValid}
      >
        {t('WITHDRAW')}
      </Button>
    </StyledForm>
  );
}

export default WithdrawForm;
