export enum CONTRACT_TYPES {
  qFee = 'q-fees-&-incentives-membership-panel',
  qDefi = 'q-defi-(decentralized-finance)-membership-panel',
  qEprs = 'q-root-node-selection-expert-panel',
  constitution = 'constitution',
  qVault = 'qVault',
  root = 'root',
  validators = 'validators',
  rootNodes = 'rootNodes',
  vesting = 'vesting',
  rootNodeSlashing = 'root-node-slashing',
  validatorNodeSlashing = 'validator-node-slashing',
  constitutionUpdate = 'constitution-update',
  generalQUpdate = 'general-q-update',
  emergencyUpdate = 'emergency-update',
  addAnewRootNode = 'add-a-new-root-node',
  removeACurrentRootNode = 'remove-a-current-root-node',
  addNewExpert = 'add-a-new-expert',
  removeCurrentExpert = 'remove-a-current-expert',
  parameterVote = 'parameter-vote',
  member = 'member',
  parameters = 'parameters',
  systemSurplusAuction = 'systemSurplusAuction',
  systemDebtAuction = 'systemDebtAuction',
  liquidationAuction = 'liquidationAuction'
};

export const CONTRACTS_NAMES = {
  addressVoting: 'addressVoting',
  upgradeVoting: 'upgradeVoting',
  constitutionVoting: 'constitutionVoting',
  emergencyUpdateVoting: 'emergencyUpdateVoting',
  generalUpdateVoting: 'generalUpdateVoting',
  rootsVoting: 'rootNodesMembershipVoting',
  rootNodesSlashingVoting: 'rootNodesSlashingVoting',
  validatorsSlashingVoting: 'validatorsSlashingVoting',

  ePQFIMembershipVoting: 'epqfiMembershipVoting',
  ePDRMembershipVoting: 'epdrMembershipVoting',

  ePQFIParametersVoting: 'epqfiParametersVoting',
  ePDRParametersVoting: 'epdrParametersVoting',

  ePRSMembershipVoting: 'eprsMembershipVoting',
  ePRSParametersVoting: 'eprsParametersVoting',

  rootNodesSlashingEscrow: 'rootNodesSlashingEscrow',
  validatorsSlashingEscrow: 'validatorsSlashingEscrow',

  systemSurplusAuction: 'systemSurplusAuction',
  liquidationAuction: 'liquidationAuction',
  systemDebtAuction: 'systemDebtAuction'
};
