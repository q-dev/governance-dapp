export const TABLE_TYPES = {
  qFees: 'qfees',
  qDefi: 'defi',
  timeLocks: 'timeLocks',

  rootNodesShort: 'rootNodesShort',
  rootNodesWidened: 'rootNodesWidened',
  rootNodesMonitoring: 'rootNodesMonitoring',

  validatorsShort: 'validators-short',
  validatorsWidened: 'validators-widened',
  validatorsMonitoring: 'validators-monitoring',

  delegations: 'delegations',

  savingCryptoAssets: 'saving-crypto-assets',
  borrowCryptoAssets: 'borrow-crypto-assets'
};
