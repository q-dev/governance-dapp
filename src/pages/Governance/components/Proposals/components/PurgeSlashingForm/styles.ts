import styled from 'styled-components';

export const StyledPurgeSlashingForm = styled.form`
  display: grid;
  gap: 16px;
  width: 370px;

  .purge-slashing-submit {
    width: 100%;
    margin-top: 8px;
  }
`;
