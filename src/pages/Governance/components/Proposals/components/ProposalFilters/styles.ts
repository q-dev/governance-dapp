import styled from 'styled-components';

export const FiltersWrapper = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  margin: -8px 0 16px;
  height: 40px;
`;
