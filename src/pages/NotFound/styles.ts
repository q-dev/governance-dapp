import styled from 'styled-components';

export const NotFoundContainer = styled.div`
  height: calc(100vh - 72px);
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  gap: 8px;
`;
